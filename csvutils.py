#!/usr/bin/env python

import csv
import sys
import operator
import itertools

from optparse import OptionParser


class CsvFilter(object):
    def __init__(self, csvinput, options):
        self.input = csvinput
        self.options = options

    def __iter__(self):
        return self()

    def __call__(self):
        for row in self.input:
            yield row

    def next(self):
        return self.input.next()


class CsvCat(CsvFilter):
    pass


class CsvGrep(CsvFilter):

    def select_row(self, row):
        return True

    def __call__(self):
        for row in self.input:
            if self.select_row(row):
                yield row


class CsvHead(CsvFilter):

    def __call__(self):
        for i, row in enumerate(self.input):
            if i > self.options.limit:
                return
            yield row


class CsvTail(CsvFilter):

    def __call__(self):
        tailrows = []
        for row in self.input:
            tailrows.append(row)
            if len(tailrows) > self.options.limit:
                tailrows.pop(0)

        for row in tailrows:
            yield row


class CsvSort(CsvFilter):

    def __call__(self):
        fields = self.options.fields.split(',')
        for row in sorted(self.input, key=operator.itemgetter(*fields)):
            yield row


class CsvUniq(CsvFilter):

    def __call__(self):
        prevrow = None
        for row in self.input:
            if row != prevrow:
                yield row
                prevrow = row


class CsvCut(CsvFilter):
    pass


def main():
    usage = "usage: %prog [options] filename.csv [filenam2.csv ...]"

    parser = OptionParser(usage=usage, version="%prog 0.1")

    parser.add_option("-d", "--delimiter", dest="delimiter", \
            default=';', help="database name")
    parser.add_option("-o", "--output", dest="output", \
            help="database name")
    (options, args) = parser.parse_args()

    if not args:
        args = [sys.stdin]

    if options.output:
        output_file = open(options.output, 'wb')
    else:
        output_file = sys.stdout

    readers = []
    for arg in args:
        reader = csv.DictReader(arg, \
                dialect='excel', delimiter=options.delimiter)
        readers.append(reader)

    iter_output = CsvFilter(itertools.chain(*readers), options)

    # get first row to get fieldnames
    first_row = iter_output.next()
    fieldnames = first_row.fields()

    writer = csv.DictWriter(output_file, fieldnames=fieldnames, \
                dialect='excel', delimiter=options.delimiter)
    writer.writerow(first_row)

    for row in iter_output:
        writer.writerow(row)

if __name__ == '__main__':
    main()
